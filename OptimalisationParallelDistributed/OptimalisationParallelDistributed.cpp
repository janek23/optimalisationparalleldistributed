#include "pch.h"
#include <iostream>
#include <random>
#include <atomic>
#include <mutex>
#include <cassert>
#include <omp.h>
#include <sstream>   

//equation cos(20x1)+cos(20x2)+0,1* (sqrt(x1^2 +x2^2))

using T = double;

constexpr T dzeta= 0.0005; //bigger dzeta == faster calculation, but worst quality
constexpr T downLimit = -10, upperLimit = 10;
constexpr T stop = 10e-3;
constexpr T max = std::numeric_limits<T>::max();

class Point {
public:
    T x = max;
    T y = max;

    Point() {}

    Point(T x, T y) {
        this->x = x;
        this->y = y;
    }
};

std::random_device rd;
std::mt19937 generator(rd());

inline T getRandomInRange(const int dLimit=downLimit, const int upLimit=upperLimit)
{
    //lock here is not needed
    std::uniform_real_distribution<> uniform(dLimit, upLimit);
    return uniform(generator);
}

Point getGradient(T x,T y)
{
    const T sqrt = std::sqrt(x * x + y * y);
    return Point(
        (0.1 *x)/sqrt  -20 * std::sin(20 * x),
        (0.1* y)/sqrt -  20 *sin(20*y)
    );
}

T getFunction(const T x, const T y) 
{
    return std::cos(20 * x) + std::cos(20 * y) + 0.1 * std::sqrt((x * x + y * y));
}

//multithread shared data
std::mutex _mtxBestFoundPoint;
Point bestFoundPoint{}; //used for multithread && openMP
std::atomic<T> bestFoundFunction = max;

std::atomic<int> activeThreads = 0;
std::atomic<long long> globalIterations = 0;

void task(const T xMin, const T xMax, const T yMin, const T yMax, const T dzeta, const T stop, const size_t threadNumber, bool ifEnableLock = true)
{
    activeThreads++;
    std::cout << "\tThread: " << threadNumber << " begin"<<std::endl;
    Point gradient{};

    auto point = Point(getRandomInRange(xMin, xMax), getRandomInRange(yMin, yMax));
    T function = max, oldFunction = max;

    int localIterations = 0;

    while (std::abs(gradient.x) > stop || std::abs(gradient.y) > stop)
    {
        oldFunction = function;
        function = getFunction(point.x, point.y);

        if (function >= oldFunction) {
            if (function < bestFoundFunction) {
                bestFoundFunction = function;

                {
                    if(ifEnableLock) std::scoped_lock sl(_mtxBestFoundPoint); //ifEnableLock used to disable lock in OpenMP
                    bestFoundPoint = point;
                }

                std::cout << "\tThread: "<<threadNumber<< " local iteration:" << localIterations << " best global Function: " << bestFoundFunction << "  bestPoint: x:" << point.x << " y:" << point.y << std::endl;
            }
            point = Point(getRandomInRange(xMin, xMax), getRandomInRange(yMin, yMax));
        }

        gradient = getGradient(point.x, point.y);

        point.x = point.x - dzeta * gradient.x * function;
        point.y = point.y - dzeta * gradient.y * function;

        localIterations++;
        //if (localIterations % 100000 == 0) std::cout <<"Thread: "<<threadNumber<< " iteration: " << localIterations << std::endl;

        globalIterations++;
        //if (globalIterations % 100000 == 0) std::cout << "Global iterations: "<<  globalIterations  << std::endl;
    }
    std::cout << "\tThread: " << threadNumber << " end after "<<localIterations<< " iterations. Total global iterations: "<<globalIterations<<std::endl;
    activeThreads--;
}


void multiThreadsImplementation(const int threads) 
{
    bestFoundPoint= Point{};
    bestFoundFunction = {};
    globalIterations = {};

    T cubeLength = std::abs(upperLimit - downLimit);
    T yMax = upperLimit;
    T yMin = downLimit;

    std::vector<std::thread> threadPool;
    for (size_t i = 0; i < threads; i++)
    {
        T xMin = downLimit + (cubeLength / (T)threads)* T(i) ;
        T xMax = xMin + (cubeLength / (T)threads);

        assert(xMax <= upperLimit+0.001f);

        threadPool.push_back(
            std::thread(
                [=]() {
                task(xMin, xMax, yMin, yMax, dzeta, stop, i); 
                })
        );
    }

    while (activeThreads > 0) {
    //wait for task to end 
        std::this_thread::sleep_for(std::chrono::milliseconds(50));
    }

    for (auto& thr : threadPool) {
        if (thr.joinable())
            thr.join();
    }
    {
        std::scoped_lock(_mtxBestFoundPoint);
        std::cout << "MultiCore bestFunction: " << bestFoundFunction << "  bestPoint: x:" << bestFoundPoint.x << " y:" << bestFoundPoint.y << std::endl;
    }
}

void singleThreadImplementation() 
{
    bestFoundPoint = Point{};
    bestFoundFunction = {};
    globalIterations = {};

    std::cout << "Single thread START" << std::endl;
    task(downLimit, upperLimit, downLimit, upperLimit, dzeta, stop, 0, false); 
    std::cout << "Single thread END" <<std::endl;
}

void openMPImplementation(const int pieces)
{
    std::cout << "OPEN MP. Available threads: " << omp_get_thread_num() << std::endl;

    bestFoundPoint = {};
    bestFoundFunction = {};
    globalIterations = {};

    if (pieces <= 0)
        throw std::exception("Wrong OMP pieces param");

    T cubeLength = std::abs(upperLimit - downLimit);
    T yMax = upperLimit;
    T yMin = downLimit;

    #pragma omp parallel for shared (bestFoundPoint)
    for(int i =0; i<pieces; i++)
    {
        const int threadNum = omp_get_thread_num();
        std::cout << "\tOMP begin thread: " << threadNum << " piece: " << i << std::endl;
        T xMin = downLimit + (cubeLength / (T)pieces) * T(i);
        T xMax = xMin + (cubeLength / (T)pieces);

        task(xMin, xMax, yMin, yMax, dzeta, stop, threadNum, false);
    }

    std::cout << "OpenMP bestFunction: " << bestFoundFunction << "  bestPoint: x:" << bestFoundPoint.x << " y:" << bestFoundPoint.y << std::endl;
}

void log(std::stringstream& ss, std::chrono::duration<double, std::milli> duration, std::string what) {
    ss << what <<
        "\n\t best function value: " << bestFoundFunction <<
        "\n\t best found point: " << bestFoundPoint.x << ", " << bestFoundPoint.y <<
        "\n\t duration ms: " << duration.count() <<
        "\n\t total iterations: " << globalIterations <<
        "\n\t avg one iter duration ns: " << duration.count() * 1000000 / globalIterations <<
        "\n";
}

int main()
{
    std::chrono::steady_clock::time_point start, end;
    std::chrono::duration<double, std::milli> duration;
    std::stringstream ss;

    ////single thread 
    start = std::chrono::high_resolution_clock::now();
    singleThreadImplementation();
    end = std::chrono::high_resolution_clock::now();
    duration = end - start;
    log(ss, duration, "SingleThread");

    ////multi threads 
    start = std::chrono::high_resolution_clock::now();
    multiThreadsImplementation(12);
    end = std::chrono::high_resolution_clock::now();
    duration = end - start;
    log(ss, duration, "MultiThreads");

    //OpenMP
    start = std::chrono::high_resolution_clock::now();
    openMPImplementation(12);
    end = std::chrono::high_resolution_clock::now();
    duration = end - start;
    log(ss, duration, "OpenMP");

    std::cout << "\n\n\n" << ss.str();



}

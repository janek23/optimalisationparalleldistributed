# OptimalisationParallelDistributed

![task](zadanie.png "Task")

![function](function.png "Function")




**Gradient funkcji:**
```math
grad(0.1\sqrt{x^2 + y^2} + cos(20 x) + cos(20 y)) = \frac{0.1 x}{\sqrt{x^2 + y^2}} - 20 sin(20 x), \frac{0.1 y}{\sqrt{x^2 + y^2}} - 20 sin(20 y)
```

**Benchmark**

CPU:  i7-8750H 6C/12T


dzeta= 0.0005\
stop = 10e-3

*Release*

```
SingleThread
         best function value: -1.97538
         best found point: -0.15583, -0.160212
         duration ms: 5701.91
         total iterations: 53121909
         avg one iter duration ns: 107.336
MultiThreads
         best function value: -1.97717
         best found point: 0.158649, -0.157231
         duration ms: 33604.2
         total iterations: 1242954538
         avg one iter duration ns: 27.0358
OpenMP
         best function value: -1.9777
         best found point: 0.156592, 0.156281
         duration ms: 25247.9
         total iterations: 984150868
         avg one iter duration ns: 25.6545
```



*Debug*

```
SingleThread
         best function value: -1.97683
         best found point: 0.158384, 0.155283
         duration ms: 63459.8
         total iterations: 255416462
         avg one iter duration ns: 248.456
MultiThreads
         best function value: -1.97757
         best found point: -0.156703, -0.155849
         duration ms: 24327.5
         total iterations: 515745941
         avg one iter duration ns: 47.1695
OpenMP
         best function value: -1.97763
         best found point: 0.156249, -0.157546
         duration ms: 55083.8
         total iterations: 879861960
         avg one iter duration ns: 62.6051
```
